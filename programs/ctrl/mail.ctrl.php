<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/mail.ui.php';
require_once dirname(__FILE__).'/../set/mail.class.php';



/**
 * 
 */
class mailspooler_CtrlMail extends mailspooler_Controller
{

	/**
	 * Returns the criteria on the specified RecordSet corresponding
	 * to the filter array.
	 *
	 * @param	array				$filter
	 * @param	mailspooler_MailSet	$mailSet
	 * 
	 * @return ORM_Criteria
	 */
	protected function getFilterCriteria($filter, $mailSet)
	{
		// Initial conditions are base on read access rights.
		$conditions = new ORM_TrueCriterion();

		if (isset($filter['_search_']) && !empty($filter['_search_'])) {

			$conditions = $conditions->_AND_(
			    $mailSet->mail_subject->contains($filter['_search_'])
			    ->_OR_($mailSet->body->contains($filter['_search_']))
			    ->_OR_($mailSet->recipients->contains($filter['_search_']))
            );

		} else {

    		if (isset($filter['mail_subject']) && !empty($filter['mail_subject'])) {
    			$conditions = $conditions->_AND_($mailSet->mail_subject->contains($filter['mail_subject']));
    		}

    		if (isset($filter['body']) && !empty($filter['body'])) {
    			$conditions = $conditions->_AND_($mailSet->body->contains($filter['body']));
    		}

    		if (isset($filter['sent_status']) && !empty($filter['sent_status'])) {
    		    $conditions = $conditions->_AND_($mailSet->sent_status->is($filter['sent_status']));
    		}

    		if (isset($filter['recipients']) && !empty($filter['recipients'])) {
    		    $conditions = $conditions->_AND_($mailSet->recipients->contains($filter['recipients']));
    		}

		}

		return $conditions;
	}




    /**
     * @param string[] $filter
     * @return mailspooler_MailTableView
     */
	protected function tableView($filter = null)
	{
		$mailSet = new mailspooler_MailSet();

		$tableView = new mailspooler_MailTableView();

		$tableView->addDefaultColumns($mailSet);
		$conditions = $this->getFilterCriteria($filter, $mailSet);
		$selection = $mailSet->select($conditions);

		$tableView->setDataSource($selection);
		$tableView->setPageLength(10);

		return $tableView;
	}





	/**
	 * 
	 * @param string[] $mails
	 * @return Widget_BabPage
	 */
	public function displayList($mails = null)
	{
	    if(!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
		$W = bab_Widgets();
		$page = $W->BabPage();

		mailspooler_ContextMenu($page, 'search');

		$page->setTitle(mailspooler_translate('Spooled emails'));
		
		$filter = isset($mails['filter']) ? $mails['filter'] : array('sort' => 'mail_date:down');

		$tableView = $this->tableView($filter);
		$filterPanel = $tableView->filterPanel('mails', $filter);
		$page->addItem($filterPanel);

		return $page;
	}


	
	
	/**
	 * 
	 * @param int $mail    The mail id in mailspooler.
	 */
	public function display($mail)
	{
	    if(!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
	    require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	    
	    $mailSet = new mailspooler_MailSet();
	    
	    $mail = $mailSet->get($mail);
	
	    $mail_data = unserialize($mail->mail_data);
	
	
	    $W = bab_Widgets();
	
	    $emailFrame = $W->Frame();
	
	    $metadata = $W->TableView();
	
	    $row = 0;
	
	    $metadata->addItem($W->Label(mailspooler_translate('Mail subject')), $row, 0);
	    $metadata->addItem($W->Label($mail->mail_subject), $row++, 1);
	
	    $metadata->addItem($W->Label(mailspooler_translate('Date')), $row, 0);
	    $metadata->addItem($W->Label(bab_shortDate(bab_mktime($mail->mail_date))), $row++, 1);
	
	    if (isset($mail_data['from'])) {
	        $metadata->addItem($W->Label(mailspooler_translate('From')), $row, 0);
	        $metadata->addItem($W->Label(mailspooler_arrayToString(array($mail_data['from']))), $row++, 1);
	    }
	
	    if (!empty($mail_data['to'])) {
	        $metadata->addItem($W->Label(mailspooler_translate('To')), $row, 0);
	        $metadata->addItem($W->Label(mailspooler_arrayToString($mail_data['to'])), $row++, 1);
	    }
	
	    if (!empty($mail_data['cc'])) {
	        $metadata->addItem($W->Label(mailspooler_translate('Cc')), $row, 0);
	        $metadata->addItem($W->Label(mailspooler_arrayToString($mail_data['cc'])), $row++, 1);
	    }
	
	    if (!empty($mail_data['bcc'])) {
	        $metadata->addItem($W->Label(mailspooler_translate('Bcc')), $row, 0);
	        $metadata->addItem($W->Label(mailspooler_arrayToString($mail_data['bcc'])), $row++, 1);
	    }
	
	    $metadata->addItem($W->Label(mailspooler_translate('Status')), $row, 0);
	    $metadata->addItem($W->Label($mail->getStatusText()), $row++, 1);
	
	    if (!empty($mail->error_msg)) {
	        $metadata->addItem($W->Label(mailspooler_translate('Error message')), $row, 0);
	        $metadata->addItem($W->Label($mail->error_msg), $row++, 1);
	    }
	
	
	    $emailFrame->addItem($metadata);
	
	    if ($mail->body) {
	        $body = trim($mail->body, " \r\n");
	    } else {
	        $body = bab_toHtml($mail->altbody);
	    }

	
	    $emailFrame->addItem($W->Html($body)->addClass('widget-bordered'));
	
	    if (!empty($mail_data['files'])) {
	        bab_functionality::includefile('Icons');
	        $T = @bab_functionality::get('Thumbnailer');
	        /*@var $T Func_Thumbnailer */
	        $fileFrame = $W->Frame()->addClass(Func_Icons::ICON_LEFT_48);
	
	        foreach($mail_data['files'] as $file) {
	            if (is_file($file[0])) {
	                $url = new bab_url('?tg=addon/mailspooler/mailspool');
	                $url->idx = 'download';
	                $url->email = $mail->id;
	                $url->file = abs(crc32($file[0]));
	
	
	                $icon = $W->Icon($file['1'], Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD);
	                if ($T) {
	                    $icon->setImageUrl($T->getIcon($file[0], 48, 48)->__tostring());
	                }
	
	                $fileFrame->addItem($W->Link($icon, $url->toString()));
	            }
	        }
	
	        $emailFrame->addItem($fileFrame);
	    }
	
	
	    if ($mail->smtp_trace) {
	        $emailFrame->addItem(
	            $W->Section(mailspooler_translate('SMTP log'), $W->RichText($mail->smtp_trace), 5)
	            ->setFoldable(true, true)
	        );
	    }
	
	    $emailFrame->addClass('mailspooler-mail');
	
	    if (bab_isAjaxRequest()) {
	        return $emailFrame;
	    }
	    
	    $page = $W->BabPage();
	    $addon = bab_getAddonInfosInstance('mailspooler');
	    $page->addStyleSheet($addon->getStylePath().'main.css');
	
	    $page->setTitle(mailspooler_translate('Mail'));

	    $page->addItem($emailFrame);
	    
	    $editor = new mailspooler_MailEditor($mail);
	    $page->addItem($editor);
	
	    return $page;
	}



	
	/**
	 * 
	 * @param int $mail    The mail id in mailspooler.
	 */
	public function delete($mail)
	{
	    $this->requireDeleteMethod();
	    
	    if(!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
	    $mailSet = new mailspooler_MailSet();
	     
	    $mail = $mailSet->get($mail);
	    
	    if (!isset($mail)) {
	        throw new Exception('Trying to delete an unknown email.');
	    }
	
	    $mail->delete();
	    $this->addMessage(mailspooler_translate('The email has been deleted.'));
	    return true;
	}
	
	
	
	

	/**
	 * 
	 * @param int $mail    The mail id in mailspooler.
	 */
	public function send($mail)
	{
	    $this->requireSaveMethod();
	    
	    if (!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
	    $mailSet = new mailspooler_MailSet();
	    
	    $mail = $mailSet->get($mail);
	     
		if (!isset($mail)) {
	        throw new Exception('Trying to send an unknown email.');
	    }

        try {
            $mail->send();
        } catch(ErrorException $e) {
            $this->addError($e->getMessage());
            return false;
	    }
	    
	    $this->addMessage(mailspooler_translate('The email has been sent.'));
	    return true;
	}
	
	/**
	 *
	 * @param int $mail    The mail id in mailspooler.
	 */
	public function testSend($mail)
	{
	    if (!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
	    $mailSet = new mailspooler_MailSet();
	    $mail = $mailSet->get($mail);
	    $mail->send();
	}
}


