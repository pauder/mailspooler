<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/mailincl.php';
require_once dirname(__FILE__) . '/../functions.php';

$Orm = bab_functionality::get('LibOrm');
/*@var $Orm Func_LibOrm */
$Orm->initMySql();

$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
ORM_MySqlRecordSet::setBackend($mysqlbackend);



/**
 * @property ORM_PkField        $id
 * @property ORM_StringField    $mail_hash
 * @property ORM_StringField    $mail_subject
 * @property ORM_TextField      $body
 * @property ORM_TextField      $altbody
 * @property ORM_StringField    $format
 * @property ORM_TextField      $recipients
 * @property ORM_TextField      $mail_data
 * @property ORM_IntField       $sent_status
 * @property ORM_TextField      $smtp_trace
 * @property ORM_TextField      $error_msg
 * @property ORM_DateTimeField  $mail_date
 * @property ORM_IntField       $send_inprogress
 * 
 * @method mailspooler_Mail get()
 * @method mailspooler_Mail newRecord()
 * @method mailspooler_Mail[] select()
 */
class mailspooler_MailSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('mail_hash'),
		    ORM_StringField('mail_subject'),
		    ORM_TextField('body'),
		    ORM_TextField('altbody'),
		    ORM_StringField('format'),
		    ORM_TextField('recipients'),
		    ORM_TextField('mail_data'),
		    ORM_IntField('sent_status'),
		    ORM_TextField('smtp_trace'),
		    ORM_TextField('error_msg'),
		    ORM_DateTimeField('mail_date'),
		    ORM_IntField('send_inprogress')
		);

	}

}



/**
 * @property int        $id
 * @property string     $mail_hash
 * @property string     $mail_subject
 * @property string     $body
 * @property string     $altbody
 * @property string     $format
 * @property string     $recipients
 * @property string     $mail_data
 * @property int        $sent_status
 * @property string     $smtp_trace
 * @property string     $error_msg
 * @property string     $mail_date
 * @property int        $send_inprogress
 */
class mailspooler_Mail extends ORM_MySqlRecord
{


    public function __construct(ORM_RecordSet $oParentSet = null)
    {
        if (isset($oParentSet)) {
            parent::__construct($oParentSet);
        }
    }



    public function getStatusText()
    {
        switch ($this->sent_status)
        {
            case null:	$status = mailspooler_translate('Wait for sending');	break;
            case '0':	$status	= mailspooler_translate('Failed');				break;
            case '1':	$status	= mailspooler_translate('Sent');				break;
            default:    $status	= mailspooler_translate('Unknown status');		break;
        }

        return $status;
    }


	private function addMail(&$mail, $list) {
		foreach($list as $arr) {
			$mail[] = $arr[0];
		}
	}

	/**
	 * Record mail in database from event mail infos
	 * @return string
	 */
	public function recordMail(bab_eventMail $event) {
		global $babDB;
		$addon = bab_getAddonInfosInstance('mailspooler');

		$attachments = array();

		if ($event->attachements) {

			$attachments = $event->attachements;

			$dir = new bab_Path($addon->getUploadPath().'mail/');
			$dir->createDir();


			foreach($event->attachements as $k => $arr) {
				$newname = clone $dir;
				$newname->push(md5(uniqid(rand(), true)));
				if (is_file($arr[0])) {
					copy($arr[0], $newname->tostring());
					$attachments[$k][0] = $newname->tostring();
				}
			}
		}

		$imageattachments = array();

		if (isset($event->imageattachements) && $event->imageattachements) {

			$imageattachments = $event->imageattachements;

			$dir = new bab_Path($addon->getUploadPath().'mail/');
			$dir->createDir();


			foreach($event->imageattachements as $k => $arr) {
				$newname = clone $dir;
				$newname->push(md5(uniqid(rand(), true)));
				if (is_file($arr[0])) {
					copy($arr[0], $newname->tostring());
					$imageattachments[$k][0] = $newname->tostring();
				}
			}
		}

		$recipients = array();
		$this->addMail($recipients, $event->to);
		$this->addMail($recipients, $event->cc);
		$this->addMail($recipients, $event->bcc);

		$recipients = implode(', ',$recipients);

		$data = array(
				'from'		=> $event->from,
				'sender'	=> $event->sender,
				'to'		=> $event->to,
				'cc'		=> $event->cc,
				'bcc'		=> $event->bcc,
				'files'		=> $attachments,
				'imagefiles'=> $imageattachments
			);

		$data = serialize($data);

		$smtp_trace = isset($event->smtp_trace) ? $event->smtp_trace : '';

		if ($event instanceof bab_eventAfterMailSent)
		{
			$sent_status = $event->sent_status ? '1' : '0';
			$ErrorInfo = (string) $event->ErrorInfo;

		} else {
			$sent_status = null;
			$ErrorInfo = '';
		}

		$mail_hash = md5($event->subject.$event->body.$data);

		$res = $babDB->db_query("SELECT id FROM mailspooler_mail WHERE mail_hash='".$babDB->db_escape_string($mail_hash)."'");

		if (0 < $babDB->db_num_rows($res)) {

			// ignorer le principe du hash car cela empeche de renvoyer un mail a partir de la liste des mails non envoyes
			// le hash est le meme et il ne l'ajoute pas

			bab_debug("mail creation in spooler rejected because mail_hash allready exists : ".$mail_hash);
			return $mail_hash;
		}

		$babDB->db_query("INSERT INTO mailspooler_mail
				( mail_hash, mail_subject, body, altbody, format, recipients, mail_data, sent_status, smtp_trace, error_msg, mail_date )
			VALUES
				(
					".$babDB->quote($mail_hash).",
					".$babDB->quote($event->subject).",
					".$babDB->quote($event->body).",
					".$babDB->quote($event->altBody).",
					".$babDB->quote($event->format).",
					".$babDB->quote($recipients).",
					".$babDB->quote($data).",
					".$babDB->quoteOrNull($sent_status).",
					".$babDB->quote($smtp_trace).",
					".$babDB->quote($ErrorInfo).",
					NOW()
				)
			");

		return $mail_hash;
	}



	public function getNextSendMail()
	{
		global $babDB;

		$babDB->db_query("LOCK TABLES mailspooler_mail WRITE");

		$res = $babDB->db_query("
			SELECT * FROM mailspooler_mail WHERE sent_status IS NULL AND send_inprogress = 0 LIMIT 0,1
		");

		$arr = $babDB->db_fetch_assoc($res);

		if(!$arr){
			$babDB->db_query("UNLOCK TABLES");
			return false;
		}

		$babDB->db_query("
			UPDATE mailspooler_mail SET
				send_inprogress='1'
			WHERE
				id=".$babDB->quote($arr['id'])
		);

		$babDB->db_query("UNLOCK TABLES");

		return $arr;
	}


	/**
	 * Process waiting mails
	 * @throws ErrorException
	 * @return int number of processed email
	 */
	public function sendWaiting()
	{
		$set = new mailspooler_MailSet();
		$n = 0;

		while ($arr = $this->getNextSendMail()) {
		    $email = $set->newRecord();
		    /* @var $email mailspooler_Mail */
		    $email->setValues($arr);
		    if ($email->send()) {
				$n++;
			}
		}

		return $n;
	}




    /**
     * Try to resend failed send mail
     * @throws ErrorException
     * @return int number of processed email
     */
    public function reSend()
    {
        global $babDB;

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/mailspooler/');
        $resendNumber = $registry->getValue('resendNumber', '40');

        if ($resendNumber <= 0) {
            return 0;
        }

        $res = $babDB->db_query("
        	SELECT * FROM mailspooler_mail 
            WHERE (sent_status = '0' OR (sent_status IS NULL AND send_inprogress=1 AND DATE_ADD(mail_date, INTERVAL 10 MINUTE)<NOW())) 
                AND DATE_ADD(mail_date, INTERVAL 12 HOUR)>=NOW() 
                LIMIT 0, " . $babDB->db_escape_string($resendNumber)
        );

        $set = new mailspooler_MailSet();
        $n = 0;

        while ($arr = $babDB->db_fetch_assoc($res)) {
            $email = $set->newRecord();
            $email->setValues($arr);
            if (!$email->send()) {
                break;
            }
            $n++;
        }

        return $n;
    }


	private function addRecipient(babMail $mail, $type, $arr) {
		$function = 'mail'.$type;

		foreach($arr as $recipient) {
			if (isset($recipient[1])) { /* name of recipient */
				/* Add email only if it's not empty */
				if (!empty($recipient[0])) {
					$mail->$function($recipient[0], $recipient[1]);
				}
			} else {
				/* Add email only if it's not empty */
				if (!empty($recipient[0])) {
					$mail->$function($recipient[0]);
				}
			}
		}
	}


	/**
	 * Delete mail from table
	 * @param array $arr
	 */
	public function deleteEmail(Array $arr) {

	    global $babDB;

	    $data = unserialize($arr['mail_data']);

	    foreach($data['files'] as $file) {
	        if (is_file($file[0]) && is_writable($file[0])) {
	            unlink($file[0]);
	        }
	    }
	    if(isset($data['imagefiles'])){
	        foreach($data['imagefiles'] as $file) {
	            if (is_file($file[0]) && is_writable($file[0])) {
	                unlink($file[0]);
	            }
	        }
	    }

	    $babDB->db_query('
			DELETE FROM mailspooler_mail WHERE id='.$babDB->quote($arr['id']).'
		');
	}

	/**
	 * Send email using smtp params of site settings, with the ovidentia API
	 * @throws ErrorException
	 * @return bool
	 */
    protected function sendBySiteSettings()
    {
        $babMail = bab_mail();
        
        if (!$babMail) {
            throw new ErrorException(mailspooler_translate('Mail is not configured'));
        }
        
        
        $babMail->clearAllRecipients();
        $babMail->clearReplyTo();
        
        $data = unserialize($this->mail_data);
        
        if (isset($data['from'])) {
            if (isset($data['from'][1])) {
                $babMail->mailFrom($data['from'][0], $data['from'][1]);
            } else {
                $babMail->mailFrom($data['from'][0]);
            }
        }
        
        $babMail->mailSender($data['sender']);
        
        $this->addRecipient($babMail, 'To', $data['to']);
        $this->addRecipient($babMail, 'Cc', $data['cc']);
        $this->addRecipient($babMail, 'Bcc', $data['bcc']);
        
        $babMail->mailSubject($this->mail_subject);
        $babMail->mailBody($this->body, $this->format);
        $babMail->mailAltBody($this->altbody);
        
        foreach ($data['files'] as $file) {
            if (is_file($file[0])) {
                $babMail->mailFileAttach($file[0], $file[1], $file[2]);
            }
        }
        
        if (isset($data['imagefiles'])) {
            foreach ($data['imagefiles'] as $image) {
                if (is_file($image[0])) {
                    $babMail->mailEmbeddedImage($image[0], $image[1], $image[2], $image[3], $image[4]);
                }
            }
        }
        
        $babMail->hash = $this->mail_hash;
        
        
        // call Send() without events, no mailspooler storage
        $sent_status = $babMail->mail->Send();
        $smtp_trace = isset($babMail->mail->smtp_trace) ? $babMail->mail->smtp_trace : '';
        
        if (!$sent_status) {
            
            $this->error_msg = $babMail->ErrorInfo();
            $this->smtp_trace = $smtp_trace;
            $this->save();
            
            throw new ErrorException(mailspooler_translate("Mail server error"));
        }
        
        require_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
        $event = new bab_eventAfterMailSent;
        $event->setMailInfos($babMail);
        $event->sent_status = $sent_status;
        $event->ErrorInfo = empty($babMail->mail->ErrorInfo) ? null : $babMail->mail->ErrorInfo;
        $event->smtp_trace = $smtp_trace;
        
        bab_fireEvent($event);
        
        $babMail->mailClearAttachments();
        
        return true;
    }
    
    /**
     * Convert list of recipients to mailjet format
     * @param array $recipients
     * @return array
     */
    protected function getMailjetRecipients($recipients)
    {
        $mailjetList = [];
        foreach ($recipients as $recipient) {
            $mailjetList[] = [
                'Email' => $recipient[0],
                'Name' => $recipient[1]
            ];
        }
        
        return $mailjetList;
    }
    
    /**
     * Send the email using the mailjet API if configured
     * @throws ErrorException
     * @return bool
     */
    protected function sendByMailjet()
    {
        bab_debug('Require mailjet', DBG_TRACE, 'mailjet');
        require_once dirname(__FILE__).'/../mailjet/vendor/autoload.php';
        $mj = new \Mailjet\Client(MAILJET_API_KEY, MAILJET_API_SECRET, true, ['version' => 'v3.1']);
        
        $data = unserialize($this->mail_data);
        
        bab_debug($data, DBG_TRACE, 'mailjet');
        
        if (empty($data['from'])) {
            bab_debug('No from', DBG_ERROR, 'mailjet');
            return false;    
        }
        
        if (count($data['imagefiles']) > 0) {
            // fallback to SMTP if image file
            bab_debug('Contain image file', DBG_ERROR, 'mailjet');
            return false;
        }
        
        $registry = bab_getRegistry();
        $registry->changeDirectory('/mailspooler/mailjet/');
        $today = date('Y-m-d');
        $dailyCountDate = $registry->getValue('dailyCountDate', $today);
        $dailyCount = $registry->getValue('dailyCount', 0);
        
        if ($dailyCountDate !== $today) {
            $dailyCount = 0;
            $registry->setKeyValue('dailyCountDate', $today);
        }
        
        if ($dailyCount >= MAILJET_DAILY_QUOTA) {
            bab_debug('Daily quota excedeed', DBG_ERROR, 'mailjet');
            return false;
        }
        
        bab_debug('Add to quota', DBG_TRACE, 'mailjet');
        $nbEmails = count($data['to']) + count($data['cc']) + count($data['bcc']);
        $registry->setKeyValue('dailyCount', $dailyCount + $nbEmails);
        
        $message = [
            'From' => [
                'Email' => $data['from'][0],
                'Name' => $data['from'][1]
            ],
            'To' => $this->getMailjetRecipients($data['to']),
            'Cc' => $this->getMailjetRecipients($data['cc']),
            'Bcc' => $this->getMailjetRecipients($data['bcc']),
            'Subject' => $this->mail_subject,
            'TextPart' => $this->altbody,
            'HTMLPart' => $this->body,
            'Attachments' => []
        ];
        
        foreach ($data['files'] as $file) {
            if (is_file($file[0])) {
                $message['Attachments'][] = [
                    'ContentType' => empty($file[2]) ? bab_getFileMimeType($file[0]) : $file[2],
                    'Filename' => $file[1],
                    'Base64Content' => base64_encode(file_get_contents($file[0]))
                ];
            }
        }
        
        $response = $mj->post(\Mailjet\Resources::$Email, ['body' => ['Messages' => [$message]]]);
        $sent_status = $response->success();
        $data = $response->getData();
        bab_debug($data, DBG_TRACE, 'mailjet');
        
        if (!$sent_status) {
            
            $this->error_msg = $data['Messages'][0]['Errors'][0]['ErrorMessage'];
            $this->smtp_trace = print_r($data['Messages'][0], true);
            $this->save();
            
            throw new ErrorException($this->error_msg);
        }
        
        require_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
        $event = new bab_eventAfterMailSent;   
        if (isset($data['from'])) {
            $event->from 		= $data['from'];
        }
        if (isset($data['to'])) {
            $event->to 			= $data['to'];
        }
        if (isset($data['cc'])) {
            $event->cc 			= $data['cc'];
        }
        if (isset($data['bcc'])) {
            $event->bcc 		= $data['bcc'];
        }
        if (isset($data['files'])) {
            $event->attachements = $data['files'];
        }
        $event->subject		= $this->mail_subject;
        $event->body		= $this->body;
        $event->altBody		= $this->altbody;
        $event->format		= 'text/html';
        $event->hash		= $this->mail_hash;
        $event->sent_status = $sent_status;
        $event->ErrorInfo = empty($data['Messages'][0]['Errors']) ? null : $data['Messages'][0]['Errors'][0]['ErrorMessage'];
        $event->smtp_trace = print_r($data['Messages'][0], true);
        
        bab_fireEvent($event);
        
        return true;
    }

	/**
	 * Sends the mail.
	 * @throws ErrorException
	 *
	 * @return bool
	 */
	public function send()
	{
	    bab_debug(MAILJET_API_KEY, DBG_TRACE, 'mailjet');
	    bab_debug(MAILJET_API_SECRET, DBG_TRACE, 'mailjet');
	    bab_debug(MAILJET_DAILY_QUOTA, DBG_TRACE, 'mailjet');
	    
	    if (defined('MAILJET_API_KEY') && defined('MAILJET_API_SECRET') && defined('MAILJET_DAILY_QUOTA')) {
	        $status = $this->sendByMailjet();
	        if ($status) {
	            bab_debug('Mailjet success return', DBG_TRACE, 'mailjet');
	            return $status;
	        }
	        
	        bab_debug('Mailjet no status', DBG_TRACE, 'mailjet');
	    }
	    
	    bab_debug('Send by site settings', DBG_TRACE, 'mailjet');
	    return $this->sendBySiteSettings();
	}




	/**
	 * Deletes the mail record and associated data.
	 */
	public function delete()
	{
	    $data = unserialize($this->mail_data);

	    foreach ($data['files'] as $file) {
	        if (is_file($file[0]) && is_writable($file[0])) {
	            unlink($file[0]);
	        }
	    }
	    
	    if (isset($data['imagefiles'])) {
	        foreach( $data['imagefiles'] as $file) {
	            if (is_file($file[0]) && is_writable($file[0])) {
	                unlink($file[0]);
	            }
	        }
	    }

	    $set = $this->getParentSet();
	    $set->delete($set->id->is($this->id));
	}

}