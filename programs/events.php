<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/set/mail.class.php';

/**
 * 
 * @param bab_eventBeforeMailSent $event
 * @return void
 */
function mailspooler_onBeforeMailSent(bab_eventBeforeMailSent $event)
{
	// permet d'enregistrer le mail et d'annuler son envoi automatique
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/mailspooler/');
	if ('timed' === $registry->getValue('method'))
	{
		$mail = new mailspooler_mail;
		$mail->recordMail($event);
		$event->cancel(true);
	}
}




function mailspooler_updateMailStatus($hash, $sentStatus, $errorMsg = '', $smtp_trace = '')
{
	global $babDB;
	
	$babDB->db_query("
		UPDATE mailspooler_mail SET
			sent_status=".$babDB->quote($sentStatus).",
			error_msg=".$babDB->quote($errorMsg).", 
			smtp_trace=".$babDB->quote($smtp_trace)." 
		WHERE mail_hash=".$babDB->quote($hash)
	);
	
}




function mailspooler_manageSuccessfullySentMail($hash, $smtp_trace)
{
	global $babDB;

	if (mailspooler_keepSentMail()) {
		
		mailspooler_updateMailStatus($hash, '1', '', $smtp_trace);
		
	} else {
		
		$mail = new mailspooler_mail();
		
		$res = $babDB->db_query('
			SELECT * FROM mailspooler_mail WHERE mail_hash='.$babDB->quote($hash)
		);
			
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$mail->deleteEmail($arr);
		}
	}

}




/**
 * 
 * @param bab_eventAfterMailSent $event
 * @return void
 */
function mailspooler_onAfterMailSent(bab_eventAfterMailSent $event)
{
	require_once dirname(__FILE__).'/functions.php';
	
	if ($event->hash)
	{
		// update mailspooler status or delete row if the mail is associated to a hash
		
		$smtp_trace = isset($event->smtp_trace) ? $event->smtp_trace : '';
		
		if (!$event->sent_status) {
			// Status == 0 ==> error.
			mailspooler_updateMailStatus($event->hash, $event->sent_status, $event->ErrorInfo, $smtp_trace);
			
		} else {
			
			mailspooler_manageSuccessfullySentMail($event->hash, $smtp_trace);
			
		}
		
	} else if (!$event->sent_status || mailspooler_keepSentMail()) {
		
		
		
		// if no hash, the mail is not in spooler, add it if sending failed

		$mail = new mailspooler_mail();
		
		$mail->recordMail($event);
	}

}




/**
 * 
 * @param LibTimer_eventEvery5Min $event
 * @return void
 */
function mailspooler_onEvery5Min(LibTimer_eventEvery5Min $event)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/mailspooler/');
	if ('timed' === $registry->getValue('method'))
	{
		$mail = new mailspooler_mail;
		
		try {
			$n = $mail->sendWaiting();
		} catch (ErrorException $e)
		{
			$event->log('mailspooler', 'ErrorException : '.$e->getMessage());
			$n = 0;
		}
		
		if ($n > 0)
		{
			$event->log('mailspooler', sprintf('%d mail sent', $n));
		}
	}
}


/**
 * 
 * @param LibTimer_eventEvery30Min $event
 * @return void
 */
function mailspooler_onEvery30Min(LibTimer_eventEvery30Min $event)
{
	$mail = new mailspooler_mail;
	
	try {
		$n = $mail->reSend();
	} catch (ErrorException $e)
	{
		$event->log('mailspooler', 'ErrorException : '.$e->getMessage());
		$n = 0;
	}
	
	if ($n > 0)
	{
		$event->log('mailspooler', sprintf('%d mail sent', $n));
	}
}